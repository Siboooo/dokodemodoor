![avatar](https://github.com/Siboooo/imgForMD/blob/master/DokodemoDoor/DokodemoDoor.png?raw=true)

# Introduction
This repository provides a scene of a portal which is inspired by the Dokodemo Door (Anywhere door).<br/>Check the video for a quick look.
<br/><br/>
![Sample Video](DokodemoDoorDemo.mp4){width=100% height=100%}  
This project can be seen as a starting point for developers who would like to develope passthrough-related application for Oculus Quest headset.

# Content
- [More about this repository](#more-about-this-repository)
    - [Including](#including)
    - [Key features](#key-features)
- [Usage](#usage)
    - [Try it out](#try-it-out)
    - [Use your own scene](#use-your-own-scene)
- [Environment](#environment)

# More about this repository 
## Including
This repository includes:
1. The whole package to import the game -> "DokodemoDoor.unitypackage"
2. All the main scripts utilized in the gmae -> "Scripts"

## Key features:
- See different worlds through the door.
- Change the virtual world as you wish.

# Usage
## Try it out
To try the Dokodemo Door:
1. Open a new project.
2. Click "Assets -> Import Package -> Custom Package" to import the "DokodemoDoor.unitypackage" file in this repository.

## Use your own scene
To import your own scene:
1. Prepare your own scene and related image as material.
2. find the "ScenePanel" GameObject.  
![avatar](https://github.com/Siboooo/imgForMD/blob/master/DokodemoDoor/TryUOwn1.png?raw=true)
3. Drag your scene and material in the realted list.  
![avatar](https://github.com/Siboooo/imgForMD/blob/master/DokodemoDoor/TryUOwn2.png?raw=true)
4. All done! Now you can see your own scene in the game.

# Environment
This repository is developed with Unity 2021.3.16f1c1(LTS) and Oculus Integration Toolkit 49.0
