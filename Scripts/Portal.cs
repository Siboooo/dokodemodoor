using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;
using UnityEngine.Rendering.Universal;

public class Portal : MonoBehaviour
{
    /// <summary>
    /// render the related scene when the user cross the door
    /// </summary>
    
    public string targetTag;
    public GameObject door;
    public RenderObjects InsideScene;
    public RenderObjects OutsideScene;
    public GameObject insideTheDoor;
    public GameObject outsideTheDoor;

    private void OnTriggerEnter(Collider other)
    {
        // Will not render the scene if the door is closed
        float rotatedAngle = door.transform.localEulerAngles.x;
        if (rotatedAngle < 2 || rotatedAngle > 358)
        {
            return;
        }

        // Update the scene when the collider is the camera and the moving direction is right
        if (other.CompareTag(targetTag))
        {
            Vector3 targetVelocity = other.GetComponent<VelocityEstimator>().GetVelocityEstimate();
            float angle = Vector3.Angle(transform.forward, targetVelocity);

            if(angle < 90)
            {
                InsideScene.SetActive(false);
                OutsideScene.SetActive(true);
                insideTheDoor.layer= 7;
                outsideTheDoor.layer= 8;
            }
        }
    }
}
