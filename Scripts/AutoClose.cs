using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class AutoClose : MonoBehaviour
{
    /// <summary>
    /// Auto close the door when the open angle is too big or too small
    /// </summary>
    
    public GameObject door;
    public Transform pivotPoint;
    public float maxAngle;
    public float maxAngleSpeed;
    public float autoCloseAngle;
    public float autoCloseSpeed;
    public AudioSource doorCloseSound;
    public float closeSoundTriggerAngle;

    private bool isOpened = false;
    private Vector3 rotateAxis = new Vector3(0f, 1f, 0f);

    void Update()
    {
        float rotatedAngle = door.transform.localEulerAngles.x;
        float indicateAngle = door.transform.localEulerAngles.y;

        // Play the close door sound effect when the door is closed from opened status
        if (rotatedAngle < closeSoundTriggerAngle || rotatedAngle > 360 - closeSoundTriggerAngle)
        {
            if (isOpened)
            {
                doorCloseSound.Play();
            }
            isOpened = false;
        }
        else{isOpened = true;}

        // Stop animation when the door's angle is less than 0.5 degree
        if (rotatedAngle < 0.5 || rotatedAngle > 359.5){return;}

        // Auto close the door when indicate angle is 90 degree (door is opend from 90 to 270 degree)
        if (indicateAngle - 90 < 5)
        {
            float rotateDirection = 1;
            if (rotatedAngle <= 180)
            {
                rotateDirection = -1;
            }
            door.transform.RotateAround(pivotPoint.position, rotateAxis, (float)rotateDirection * maxAngleSpeed);
            return;
        }

        // Auto close the door when the door is opened less than 90 degree or greater than 270 degree
        if (rotatedAngle != 0 && rotatedAngle < autoCloseAngle)
        {
            door.transform.RotateAround(pivotPoint.position, rotateAxis, (float)-autoCloseSpeed);
            return;
        }

        if (rotatedAngle > 360 - autoCloseAngle)
        {
            door.transform.RotateAround(pivotPoint.position, rotateAxis, (float)autoCloseSpeed);
            return;
        }

        // Auto close the door to the maximum open angle when the angle is over the limit
        if (rotatedAngle > 180 && rotatedAngle < 360 - maxAngle)
        {
            door.transform.RotateAround(pivotPoint.position, rotateAxis, (float)maxAngleSpeed);
            return;
        }

        if (rotatedAngle < 180 && rotatedAngle > maxAngle)
        {
            door.transform.RotateAround(pivotPoint.position, rotateAxis, (float)-maxAngleSpeed);
            return;
        }
    }
}
