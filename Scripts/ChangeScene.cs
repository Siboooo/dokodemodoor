using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

public class ChangeScene : MonoBehaviour
{
    /// <summary>
    /// Trigger by the button to change the scene and related image
    /// </summary>
    
    public List<Material> materials;
    public List<GameObject> relatedScenes;
    public GameObject door;
    public GameObject tips;

    private int index = 0;

    // Initialse the first scene
    public void InitScene()
    {
        foreach (GameObject scene in relatedScenes)
        {
            scene.SetActive(false);
        }
        relatedScenes[index].SetActive(true);
        // image list and scene list should have the same size
        Assert.IsTrue(materials.Count == relatedScenes.Count);
    }

    public void LastScene()
    {
        SwitchScene(1);
    }

    public void NextScene()
    {
        SwitchScene(-1);
    }

    private void SwitchScene(int direction)
    {
        // Pop the tips if door is opend
        float rotatedAngle = door.transform.localEulerAngles.x;
        if (rotatedAngle > 5 && rotatedAngle < 355)
        {
            tips.GetComponent<Animation>().Play();
            return;
        }

        // change scene and related image
        relatedScenes[index].SetActive(false);

        index += direction + relatedScenes.Count;
        index %= relatedScenes.Count;

        this.gameObject.GetComponent<Renderer>().material = materials[index];
        relatedScenes[index].SetActive(true);
    }
}
